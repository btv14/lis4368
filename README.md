> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS 4368 - Advanced Web Applications Development

## Brandon Velasquez

### Assignment # Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Git
    - Create a bitbucket account
    - Complete bitbucket tutorials
    - Install tomcat
    - Create bitbucket repo
    - Provide screenshots of installations
    - Provide short descriptions for git commands

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Download and Install MySQL
    - Finish tutorials
    - Provide working links
    - Provide query book screenshot

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create and foward engineer ERD
    - Provide working links
    - Provide ERD screenshot

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Download and modify class files
    - Perform client-side data validation
    - Provide splash page and validation screenshots

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Download and modify class files
    - Compile java class files
    - Perform server-side data validation

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Download and modify class files
    - Compile java class files
    - Add Insert Functionality
    - Perform server-side data validation

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Download and modify class files
    - Compile java class files
    - Add CRUD Functionality
    - Perform server-side data validation
    

