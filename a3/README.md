> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Brandon T. Velasquez

### Assignment 3 Requirements:

*Parts:*

1. Design ERD
2. Foward Engineer
3. Chapter Questions (Chs. 7 - 8)

#### README.md file should include the following items:

* Course title,your name,assignment requirements,as per A1
* Screenshot of ERD
* Links to files

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Links:

*MWB file*

[a3.mwb](docs/a3.mwb)

*SQL file*

[a3.sql](docs/a3.sql)



#### Assignment Screenshots:

*Screenshot of ERD*:

![Screenshot of ERD](img/a3.png)


 

