> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Brandon T. Velasquez

### Project 2 Requirements:

*Assignment Parts:*

1. Download & Modify Project Files
2. Compile files
3. Chapter Questions


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of valid form*:

![Screenshot of valid form](img/validform.png)

*Screenshot of passed validation*:

![Screenshot of passed validation](img/validation.png)

*Screenshot of data display*:

![Screenshot of data display](img/DataDisplay.png)

*Screenshot of modify form*:

![Screenshot of modify form](img/modifyform.png)

*Screenshot of modify data*:

![Screenshot of modify data](img/modifydata.png)

*Screenshot of delete form*:

![Screenshot of delete form](img/deleteform.png)

*Screenshot of crud entry*:

![Screenshot of crud entry](img/crud.png)
 

