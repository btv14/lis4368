> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Brandon T. Velasquez

### Assignment 4 Requirements:

*Assignment Parts:*

1. Download & Modify Project Files
2. Compile files
3. Chapter Questions


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of failed validation*:

![Screenshot of failed validation](img/failedvalidation.png)

*Screenshot of passed validation*:

![Screenshot of passed validation](img/passedvalidation.png)
 

