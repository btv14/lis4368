> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Brandon T. Velasquez

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs. 1 - 4)

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running http://localhost:9999
* Git commands w/short descriptions
* Bitbucket repo links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init -- creates an empty repository and reinitializes an existing one.
2. git status -- shows the working tree status.
3. git add -- add file contents to the index.
4. git commit -- record changes to the repo.
5. git push -- update remote refs along with associated objects.
6. git pull -- fetch from and integrate with another repo or a local branch.
7. git clone -- clone a repo into a new directory.

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

#### Tutorial Links:
 
*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/btv14/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/btv14/myteamquotes/ "My Team Quotes Tutorial")