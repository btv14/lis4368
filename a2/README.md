> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Brandon T. Velasquez

### Assignment 2 Requirements:

*Three Parts:*

1. MySQL Download and Installation
2. Developing and Deploying WebApp
3. Chapter Questions (Chs. 5 - 6)

#### README.md file should include the following items:

* Working Assesment links
* Screenshot of query results from http://localhost:9999/hello/querybook.html

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assesment Links:

1. [http://localhost:9999/hello](http://localhost:9999/hello) 
2. [(http://localhost:9999/hello/HelloHome.html](http://localhost:9999/hello/HelloHome.html)
3. [http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello)
4. [http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)
5. [(http://localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi)


#### Assignment Screenshots:

*Screenshot of query results*:

![Screenshot of querybook.html](img/query_results.png)

 

